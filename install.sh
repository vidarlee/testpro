#!/bin/bash

if [ -z $1 ]; then
    echo "service name should be specified"
    exit 1
fi

echo "get service status..."
sudo systemctl status $1 2>&1 | egrep 'not be found|not-found'
if [ $? -ne 0 ]; then
    echo "service restart..."
    sudo systemctl restart $1
else
    echo "make service file..."
    dir_name=`pwd`
    service_name=$1".service"
    sudo cat > /usr/lib/systemd/system/$service_name << EOF
[Unit]
Description=udp pipeline test service
After=NetworkManager.service

[Service]
Type=simple
ExecStart=/usr/bin/java -jar $dir_name/$1.jar > $dir_name/udptest.log 2>&1
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF
    echo "systemctl daemon-reload..."
    sudo systemctl daemon-reload
    echo "systemctl enable..."
    sudo systemctl enable $1 > /dev/null 2>&1
    echo "service start..."
    sudo systemctl start $1
fi

echo "check service status..."
sudo systemctl status $1 | grep active
exit $?
