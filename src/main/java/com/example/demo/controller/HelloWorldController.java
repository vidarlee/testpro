package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloWorldController {

    @GetMapping("/world")
    public String getHelloWorld() {

        return "Hello World!\nWelcome to Unilever China Devops Platform!\nFor Release-8\n";

    }

}
