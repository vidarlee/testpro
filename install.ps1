#powershell.exe -ExecutionPolicy Bypass -File install.ps1 udpTest

$ServiceName = $args[0]
$scriptpath = $MyInvocation.MyCommand.Path
$scriptdir = Split-Path $scriptpath
$servicebat = "udpservicestart.bat"
$ServiceBin =  Join-Path $scriptdir $servicebat
if (Get-Service $ServiceName -ErrorAction SilentlyContinue) 
{
   Stop-Service $ServiceName
   Start-Service $ServiceName
}
else
{
	New-Service -Name $ServiceName -DisplayName "Udp Test service" -BinaryPathName $ServiceBin -Description "For Unilever Devops Platform test" -StartupType Manual | Out-Null
	Start-Service $ServiceName
}
exit (Get-Service $ServiceName -ErrorAction SilentlyContinue)
